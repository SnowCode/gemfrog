#!/bin/python3
import requests, re, os, sys, time
from bs4 import BeautifulSoup

# Getting variables from query
query = os.environ['QUERY_STRING']
if not query:
    print("10 Enter forum name: ")
    sys.exit()

# Downloading file
try:
    file_time = os.path.getmtime(f"/tmp/{query}.html")
except:
    file_time = 0

if (time.time() - file_time) > 600:
    url = f"https://raddle.me/f/{query}"
    page = requests.get(url)
    open(f"/tmp/{query}.html", "w").write(page.text)
    soup = BeautifulSoup(page.content, "html.parser")
else:
    page = open(f"/tmp/{query}.html").read()
    soup = BeautifulSoup(page, "html.parser")

# Extracting info
links = soup.find_all("a", class_="submission__link")
usernames = soup.find_all("a", class_="submission__submitter")
comments = soup.find_all("a", string=re.compile("comments?$"))
timestamps = soup.find_all("time", class_="submission__timestamp")
scores = soup.find_all("span", class_="vote__net-score")

print("20 text/gemini")
print(f"""
# GemFrog
Welcome on the Gemini mirror for Raddle! Have fun here (experimental).

You are currently browsing {query}

=> index.py Go back to home
=> forum.py View another forum

""")

for post in range(len(links)):
    id_regex = re.compile("\d{6,}")
    post_id = id_regex.search(comments[post]['href']).group()
    print(f"=> post.py?{post_id} {scores[post].text} - {links[post].text}")
    print(f"* Submitted by {usernames[post].text} {timestamps[post].text} ({comments[post].text})")
