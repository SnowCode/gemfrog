#!/bin/python3
import requests, re, os, time
from bs4 import BeautifulSoup

# Downloading page
try:
    file_time = os.path.getmtime("/tmp/index.html")
except:
    file_time = 0

if (time.time() - file_time) > 600:
    url = "https://raddle.me"
    page = requests.get(url)
    open("/tmp/index.html", "w").write(page.text)
    soup = BeautifulSoup(page.content, "html.parser")
else:
    page = open("/tmp/index.html").read()
    soup = BeautifulSoup(page, "html.parser")

# Extracting info
links = soup.find_all("a", class_="submission__link")
usernames = soup.find_all("a", class_="submission__submitter")
comments = soup.find_all("a", string=re.compile("comments?$"))
timestamps = soup.find_all("time", class_="submission__timestamp")
forums = soup.find_all("a", class_="submission__forum")
scores = soup.find_all("span", class_="vote__net-score")

print("20 text/gemini")
print(f"""
# GemFrog
Welcome on the Gemini mirror for Raddle! Have fun here (experimental)

=> forum.py View a specific forum

""")

for post in range(len(links)):
    id_regex = re.compile("\d{6,}")
    post_id = id_regex.search(comments[post]['href']).group()
    print(f"=> post.py?{post_id} {scores[post].text} - {links[post].text}")
    print(f"* Submitted by {usernames[post].text} in {forums[post].text} {timestamps[post].text} ({comments[post].text})")
