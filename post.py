#!/bin/python3
import requests, re, os, sys, pypandoc, time
from bs4 import BeautifulSoup
from md2gemini import md2gemini

os.environ.setdefault('PYPANDOC_PANDOC', '/usr/bin/pandoc')

# Getting variables from query
query = os.environ['QUERY_STRING']
if not query:
    print("10 Enter id: ")
    sys.exit()

# Downloading page
try:
    file_time = os.path.getmtime(f"/tmp/{query}.html")
except:
    file_time = 0

if (time.time() - file_time) > 600:
    url = f"https://raddle.me/{query}"
    page = requests.get(url)
    open(f"/tmp/{query}.html", "w").write(page.text)
    soup = BeautifulSoup(page.content, "html.parser")
else:
    page = open(f"/tmp/{query}.html").read()
    soup = BeautifulSoup(page, "html.parser")

# Extracting info
link = soup.find_all("a", class_="submission__link")[0]
username = soup.find_all("a", class_="submission__submitter")[0].text.replace("/user/","")
comment = soup.find_all("a", string=re.compile("comments?$"))[0]
timestamp = soup.find_all("time", class_="submission__timestamp")[0]
forum = soup.find_all("a", class_="submission__forum")[0].text.replace("/f/", "")
score = soup.find_all("span", class_="vote__net-score")[0]

try:
    content_html = soup.find_all("div", class_="submission__body")[0].encode_contents()
    content_md = pypandoc.convert_text(content_html, "md", "html")
    content_gmi = md2gemini(content_md)
except:
    content_gmi = "No content found, probably just a link"

print("20 text/gemini")
print(f"""
# GemFrog
Welcome on the Gemini mirror for Raddle! Have fun here (experimental).

You are currently viewing a post ({query})

=> https://raddle.me/{query} View this post on Raddle (HTTPS)

=> forum.py?{forum} Go to f/{forum}
=> user.py?{username} Go to u/{username}

=> index.py Go back to home
=> forum.py View another forum

## {link.text}
""")

if link['href'].startswith("/"):
    pass
else:
    print(f"=> {link['href']} {link['href']}")

print(f"""{content_gmi}

## {comment.text}
This feature isn't implemented yet
""")
