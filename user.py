#!/bin/python3
import requests, re, os, sys, pypandoc
from bs4 import BeautifulSoup
from md2gemini import md2gemini

os.environ.setdefault('PYPANDOC_PANDOC', '/usr/bin/pandoc')

# Getting variables from query
query = os.environ['QUERY_STRING']
if not query:
    print("10 Enter user name: ")
    sys.exit()

# Downloading page
url = f"https://raddle.me/user/{query}"
page = requests.get(url)
soup = BeautifulSoup(page.content, "html.parser")
#page = open("page.html", "r").read() # temp avoid spam on the server
#soup = BeautifulSoup(page, "html.parser")

# Extracting info
bio_html = soup.find_all("div", class_="user-bio__biography")[0]
bio_md = pypandoc.convert_text(bio_html, "md", "html")
bio_gmi = md2gemini(bio_md)

print("20 text/gemini")
print(f"""
# GemFrog
Welcome on the Gemini mirror for Raddle! Have fun here (experimental).

You are currently browsing u/{query}

=> index.py Go back to home

> This feature is currently in beta, more will come later. But here's the bio of u/{query}

{bio_gmi}
""")

